<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Redirect;
use View;

class TickerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch($request->submitbutton) {

            case 'Get Tickers': 
                
                $ticker = $this->getTickers($request->input("marketName"));
                if($ticker['success']){
                    return view('ticker', [
                        'Bid' => $ticker['result']['Bid'],
                        'Ask' => $ticker['result']['Ask'],
                        'Last' => $ticker['result']['Last']
                    ]);
                }else{
                    return view('ticker')->with('No Data Found');
                }
                break;
        
            case 'Get Best Selling Price': 
                
                $bestSellingPriceArray = $this->getBestSellPrice($request->input("marketName"));
                
                if($bestSellingPriceArray["status"] == "Success"){
                    return view('ticker')->with('bestSellingPriceArray', $bestSellingPriceArray);
                }else{
                    return view('ticker')->with('Invalid Symbol Pair');
                }
                break;
        }
    }

    public function getTickers($marketName){
        
         
        $url = "https://api.bittrex.com/api/v1.1/public/getticker?market=".$marketName;
        //$url = "https://cex.io/api/ticker/".$market1."/".$market2;
        //  Initiate curl
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL,$url);
        // Execute
        $result=curl_exec($ch);
        // Closing
        curl_close($ch);

        // Will dump a beauty json :3
        $ticker = json_decode($result, true);
        //dd($ticker);
        return $ticker;
    }
    
    public function getBestSellPrice($marketName){
        $resultArray = array();
        
        $bittrexUrl = "https://api.bittrex.com/api/v1.1/public/getticker?market=".$marketName;

        $marketNames = explode("-", $marketName);
        
        $cexUrl = "https://cex.io/api/ticker/".$marketNames[1]."/".$marketNames[0];
        
        //  Initiate curl
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $bittrexUrl);
        // Execute
        $result1 = curl_exec($ch);
        
        $ticker1 = json_decode($result1, true);
        
        // check the result of the first request
        if($ticker1['success']){
            // if the result dictates that you make another request, update the url
            curl_setopt($ch, CURLOPT_URL, $cexUrl);

            // execute the second request
            $result2 = curl_exec($ch);
            $ticker2 = json_decode($result2, true);
            
        }
        // Closing
        curl_close($ch);
        
        if(array_key_exists('error', $ticker2)) {
            $resultArray["status"] = "Failed";
        } else {
            $bestSellingPrice = max($ticker1['result']['Ask'], $ticker2['ask']);
            $sellingPriceArray = array();
            $sellingPriceArray["API"] = ($ticker1['result']['Ask'] == $bestSellingPrice) ? "Bittrex" : "Cex" ;
            $sellingPriceArray["MaxSellingPrice"] = $bestSellingPrice;
            
            $resultArray["status"] = "Success";
            $resultArray["bittrex"] = $ticker1['result'];
            $resultArray["cex"]["Bid"] = $ticker2["bid"];
            $resultArray["cex"]["Ask"] = $ticker2["ask"];
            $resultArray["cex"]["Last"] = $ticker2["last"];
            $resultArray["sellingPrice"] = $sellingPriceArray;

            return $resultArray;
        }
    }

    public function getMarketNames(){
        $url = "https://api.bittrex.com/api/v1.1/public/getmarkets";
        
        //  Initiate curl
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL,$url);
        // Execute
        $result=curl_exec($ch);
        // Closing
        curl_close($ch);

        $marketNames = json_decode($result, true);
        if($marketNames['success']){

            return view('welcome')->with('marketNames', $marketNames['result']);

        }else{
            return view('welcome')->with('No Data Found');
        }
    }
}
